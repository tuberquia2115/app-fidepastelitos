import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ActivityIndicator,
  Animated,
  StyleProp,
  ImageStyle,
  NativeSyntheticEvent,
  ImageErrorEventData,
} from 'react-native';
import {useAnimated} from '../hooks/useAnimated';
import {theme} from '../theme/appTheme';

interface Props {
  uri: string;
  style: StyleProp<ImageStyle>;
}
export const FadeInImage = ({uri, style = {}}: Props) => {
  const {opacity, fadeIn} = useAnimated();
  const [isLoading, setIsLoading] = useState(true);

  const finishLoading = () => {
    setIsLoading(false);
    fadeIn(1000);
  };

  const onError = (err: NativeSyntheticEvent<ImageErrorEventData>) => {
    setIsLoading(false);
  };
  return (
    <View style={styles.container}>
      {isLoading && (
        <ActivityIndicator
          style={styles.activity}
          color={theme.primary.main}
          size={35}
        />
      )}
      <Animated.Image
      resizeMode="contain"
      blurRadius={0}
      defaultSource={require('../assets/img/logo.png')}
        source={{uri}}
        style={{
          ...(style as any),
          opacity,
        }}
        onLoadEnd={finishLoading}
        onError={onError}
      />
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  activity: {
    position: 'absolute',
  },
});

import React from 'react';
import {View, Image, StyleSheet} from 'react-native';

export const Logo = () => {
  return (
    <View style={styles.container}>
      <Image source={require('../assets/img/logo.png')} style={styles.img} resizeMode="cover" />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  img: {
    width: 200,
    height: 200,
    
  },
});

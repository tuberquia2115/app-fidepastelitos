import React from 'react';

import {View, StyleSheet} from 'react-native';
import {theme} from '../theme/appTheme';

export const Background = () => {
  return <View style={styles.container} />;
};

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: -250,
    bottom: 200,
    width: 1000,
    height:1200,
    backgroundColor: theme.primary.light,
    transform: [{rotate: '-70deg'}],
  },
});

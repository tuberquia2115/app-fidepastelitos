import React from 'react';
import {
  Text,
  StyleProp,
  ViewStyle,
  StyleSheet,
  TextStyle,
} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {theme} from '../theme/appTheme';

interface Props {
  label: string;
  color?: 'primary' | 'secondary';
  onPress: () => void;
  stylesBtn?: StyleProp<ViewStyle>;
  stylesLabel?: StyleProp<TextStyle>;
}
export const Button = ({
  label,
  color = 'primary',
  onPress,
  stylesBtn = {},
  stylesLabel = {},
}: Props) => {
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      onPress={onPress}
      style={{
        ...(stylesBtn as any),
        ...styles.btn,
        backgroundColor: theme[color].main,
        shadowColor: `${theme[color].main}`,
      }}>
      <Text
        style={{
          ...(stylesLabel as any),
          ...styles.label,
        }}>
        {label}
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  btn: {
    padding: 10,
    marginVertical: 10,
    borderRadius: 20,
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
  },
  label: {
    color: 'white',
    fontSize: 20,
    textAlign: 'center',
    fontFamily: 'Arial',
    fontWeight: 'bold',
    textTransform: 'capitalize',
  },
});

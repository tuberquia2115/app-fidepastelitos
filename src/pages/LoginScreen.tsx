import React, {useContext, useEffect} from 'react';
import {
  View,
  Text,
  TextInput,
  Platform,
  TouchableOpacity,
  KeyboardAvoidingView,
  Keyboard,
  Alert,
  TouchableWithoutFeedback,
} from 'react-native';

import {Background} from '../components/Background';
import {Logo} from '../components/Logo';
import {loginStyles} from '../theme/loginTheme';
import {useForm} from '../hooks/useForm';
import {StackScreenProps} from '@react-navigation/stack';
import {AuthContext} from '../context/auth/authContext';

interface Props extends StackScreenProps<any, any> {}

export const LoginScreen = ({navigation}: Props) => {
  const {signIn, errorMessage, removeError} = useContext(AuthContext);
  const {email, password, onChange} = useForm({
    email: '',
    password: '',
  });

  const onSubmit = () => {
    Keyboard.dismiss();
    signIn({correo: email, password});
  };

  useEffect(() => {
    if (errorMessage.trim() === '') return;

    Alert.alert('Información Incorrecta', errorMessage, [
      {
        text: 'Ok',
        onPress: removeError,
      },
    ]);
  }, [errorMessage]);

  return (
    <>
      <Background />
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
        style={{flex: 1}}>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <View style={loginStyles.formContainer}>
            <Logo />

            <Text style={loginStyles.title}>Login</Text>

            <Text style={loginStyles.label}>Email:</Text>
            <TextInput
              placeholder="ingrese su Email"
              placeholderTextColor="rgba(255,255,255,0.4)"
              keyboardType="email-address"
              underlineColorAndroid="white"
              style={[
                loginStyles.inputField,
                Platform.OS === 'ios' && loginStyles.inputFieldIOS,
              ]}
              selectionColor="white"
              value={email}
              onChangeText={value => onChange(value, 'email')}
              onSubmitEditing={onSubmit}
              autoCapitalize="none"
              autoCorrect={false}
            />
            <Text style={loginStyles.label}>Contraseña:</Text>
            <TextInput
              textContentType="password"
              secureTextEntry
              placeholder="******"
              placeholderTextColor="rgba(255,255,255,0.4)"
              underlineColorAndroid="white"
              style={[
                loginStyles.inputField,
                Platform.OS === 'ios' && loginStyles.inputFieldIOS,
              ]}
              selectionColor="white"
              value={password}
              onChangeText={value => onChange(value, 'password')}
              onSubmitEditing={onSubmit}
            />

            {/**Bottón login */}
            <View style={loginStyles.buttonContainer}>
              <TouchableOpacity
                activeOpacity={0.8}
                style={loginStyles.button}
                onPress={onSubmit}>
                <Text style={loginStyles.buttonText}>Iniciar Sesión</Text>
              </TouchableOpacity>
            </View>

            {/**crear una nueva cuenta */}
            <View style={loginStyles.newUserContainer}>
              <TouchableOpacity
                activeOpacity={0.8}
                onPress={() => navigation.replace('RegisterScreen')}>
                <Text style={loginStyles.buttonText}>Crear nueva cuenta</Text>
              </TouchableOpacity>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    </>
  );
};

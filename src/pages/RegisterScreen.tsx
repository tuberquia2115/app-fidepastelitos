import React, { useEffect } from 'react';
import {
  View,
  Text,
  Platform,
  KeyboardAvoidingView,
  TextInput,
  TouchableOpacity,
  Keyboard,
  Alert,
  TouchableWithoutFeedback,
} from 'react-native';
import { StackScreenProps } from '@react-navigation/stack';
import Icon from 'react-native-vector-icons/Ionicons';

import { Logo } from '../components/Logo';
import { theme } from '../theme/appTheme';
import { loginStyles } from '../theme/loginTheme';
import { useForm } from '../hooks/useForm';
import { useContext } from 'react';
import { AuthContext } from '../context/auth/authContext';

interface Props extends StackScreenProps<any, any> { }

export const RegisterScreen = ({ navigation }: Props) => {
  const { signUp, errorMessage, removeError } = useContext(AuthContext);

  const { name, email, password, onChange } = useForm({
    name: '',
    email: '',
    password: '',
  });

  const onSubmit = () => {
    signUp({ nombre: name, correo: email, password });
    Keyboard.dismiss();
  };

  useEffect(() => {
    if (errorMessage.trim() === '') return;

    Alert.alert('Registro Incorrecto', errorMessage, [
      {
        text: 'Ok',
        onPress: removeError,
      },
    ]);
  }, [errorMessage]);


  return (
    <>
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
        style={{ flex: 1, backgroundColor: theme.primary.light }}>
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>

        <View style={loginStyles.formContainer}>
          <Logo />

          <Text style={loginStyles.title}>Registro</Text>

          <Text style={loginStyles.label}>Numbre:</Text>
          <TextInput
            placeholder="Ingrese su nombre"
            placeholderTextColor="rgba(255,255,255,0.4)"
            underlineColorAndroid="white"
            style={[
              loginStyles.inputField,
              Platform.OS === 'ios' && loginStyles.inputFieldIOS,
            ]}
            selectionColor="white"
            value={name}
            onChangeText={value => onChange(value, 'name')}
            onSubmitEditing={onSubmit}
            autoCapitalize="words"
            autoCorrect={false}
            />

          <Text style={loginStyles.label}>Email:</Text>
          <TextInput
            placeholder="ingrese su Email"
            placeholderTextColor="rgba(255,255,255,0.4)"
            keyboardType="email-address"
            underlineColorAndroid="white"
            style={[
              loginStyles.inputField,
              Platform.OS === 'ios' && loginStyles.inputFieldIOS,
            ]}
            selectionColor="white"
            value={email}
            onChangeText={value => onChange(value, 'email')}
            onSubmitEditing={onSubmit}
            autoCapitalize="none"
            autoCorrect={false}
            />

          <Text style={loginStyles.label}>Contraseña:</Text>
          <TextInput
            textContentType="password"
            secureTextEntry
            placeholder="******"
            placeholderTextColor="rgba(255,255,255,0.4)"
            underlineColorAndroid="white"
            style={[
              loginStyles.inputField,
              Platform.OS === 'ios' && loginStyles.inputFieldIOS,
            ]}
            selectionColor="white"
            value={password}
            onChangeText={value => onChange(value, 'password')}
            onSubmitEditing={onSubmit}
            />

          {/**Bottón login */}
          <View style={loginStyles.buttonContainer}>
            <TouchableOpacity
              activeOpacity={0.8}
              style={loginStyles.button}
              onPress={onSubmit}>
              <Text style={loginStyles.buttonText}>Registrarme</Text>
            </TouchableOpacity>
          </View>

          {/**crear una nueva cuenta */}

          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() => navigation.replace('LoginScreen')}
            style={loginStyles.buttonReturn}>
            <Icon name="arrow-undo-circle-outline" size={50} color="white" />
          </TouchableOpacity>
        </View>
              </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    </>
  );
};

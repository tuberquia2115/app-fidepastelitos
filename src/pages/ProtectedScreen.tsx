import React, {useContext} from 'react';
import {View, Text, Button, StyleSheet} from 'react-native';
import {AuthContext} from '../context/auth/authContext';

export const ProtectedScreen = () => {
  const {logOunt, user, token} = useContext(AuthContext);
  return (
    <View style={styles.container}>
      <Text>Usario: {user?.nombre}</Text>
      <Button title="Salir" onPress={logOunt} />

      <Text>{JSON.stringify(user, null, 5)}</Text>
      <Text>TOKEN: {token}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 20
  },
});

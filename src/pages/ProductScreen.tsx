import React, {useContext, useEffect, useLayoutEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TextInput,
  Alert,
  Animated,
} from 'react-native';
import {Picker} from '@react-native-picker/picker';
import Icon from 'react-native-vector-icons/Ionicons';
import {StackScreenProps} from '@react-navigation/stack';
import {
  ImagePickerResponse,
  launchCamera,
  launchImageLibrary,
} from 'react-native-image-picker';

import {ProductsStackRootParams} from '../navigator/ProductsNavigator';
import {theme} from '../theme/appTheme';
import {useCategories} from '../hooks/useCategories';
import {useForm} from '../hooks/useForm';
import {ProductsContext} from '../context/products/productsContext';
import {useAnimated} from '../hooks/useAnimated';
import {FadeInImage} from '../components/FadeInImage';

interface Props
  extends StackScreenProps<ProductsStackRootParams, 'ProductScreen'> {}

export const ProductScreen = ({navigation, route}: Props) => {
  const [colorFocus, setColorFocus] = useState('rgba(0,0,0,0.2)');
  const [tempUri, setTempUri] = useState<string>('');
  const {id = '', name = ''} = route.params;
  const {
    loadProductById,
    addProduct,
    updateProduct,
    deleteProduct,
    uploadImage,
  } = useContext(ProductsContext);

  const {opacity, position, fadeIn, startMovingPosition} = useAnimated();
  const {categories} = useCategories();
  const {_id, categoryId, nombre, img, messageError, onChange, setFormValue} =
    useForm({
      _id: id,
      categoryId: '',
      nombre: name,
      img: '',
      messageError: '',
    });

  const onFocus = () => {
    setColorFocus(theme.secondary.dark);
  };

  useLayoutEffect(() => {
    navigation.setOptions({
      title: !nombre ? 'Producto Sin Nombre' : nombre,
      headerBackImage: ({tintColor}) => (
        <Icon
          name="arrow-undo-outline"
          size={35}
          color={tintColor}
          style={{marginLeft: 10}}
        />
      ),
      headerBackTitleVisible: false,
      headerRight: !_id
        ? undefined
        : () => (
            <Icon
              name="trash-outline"
              size={35}
              color={theme.primary.dark}
              style={{marginRight: 10}}
              onPress={deleteProd}
            />
          ),
    });
  }, [nombre]);

  useEffect(() => {
    loadProduct();
  }, []);

  const loadProduct = async () => {
    fadeIn(1000);
    startMovingPosition(200);
    if (id.length === 0) return;
    const product = await loadProductById(id);
    setFormValue({
      _id: id,
      categoryId: product.categoria._id,
      nombre: product.nombre,
      img: product.img || '',
      messageError: '',
    });
  };

  const deleteProd = () => {
    Alert.alert(
      'Esta seguro de eleminar el producto',
      'esto no se puede revertir',
      [
        {style: 'cancel', text: 'Cancelar'},
        {
          style: 'destructive',
          text: 'Borrar',
          onPress: async () => {
            await deleteProduct(id);
            alert('delete');
          },
        },
      ],
    );
  };

  const alert = (type: string) => {
    const messages: any = {
      create: 'El producto se creo correctamente',
      update: 'El producto se actualizó correctamente',
      delete: `El Producto ${nombre} se eleminó correctamente`,
      error: 'El nombre es obligatorio',
    };
    Alert.alert('', messages[type], [
      {
        text: 'OK',
        style: 'default',
        onPress: () => navigation.goBack(),
      },
    ]);
  };

  const saveOrUpdate = async () => {
    if (id.length > 0) {
      await updateProduct(categoryId, nombre, id);
    } else {
      if (!nombre) {
        return onChange('Nombre obligatorio', 'messageError');
      }
      const tempCategoryId = categoryId || categories[0]._id;
      const newProduct = await addProduct(tempCategoryId, nombre);
      fadeIn(1000);
      startMovingPosition(200);
      onChange(newProduct._id, '_id');
    }
  };

  const upload = (resp: ImagePickerResponse) => {
    if (resp.didCancel) return;
    if (resp.assets && resp.assets.length > 0 && resp.assets[0].uri) {
      setTempUri(resp.assets[0].uri);
      uploadImage(resp, _id);
    }
  };

  const takePhoto = () => {
    launchCamera(
      {
        mediaType: 'photo',
        quality: 0.5,
        cameraType: 'back',
      },
      upload,
    );
  };

  const takePhotoFromGallery = () => {
    launchImageLibrary(
      {
        mediaType: 'photo',
        quality: 0.5,
      },
      upload,
    );
  };

  return (
    <View style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Text style={styles.label}>Nombre Producto</Text>
        <TextInput
          placeholder="Nombre del producto"
          autoCorrect={false}
          value={nombre}
          onChangeText={value => onChange(value, 'nombre')}
          style={{...styles.textInput, borderColor: colorFocus}}
          onFocus={onFocus}
          autoCapitalize="words"
        />
        {messageError.length > 0 && (
          <Text style={styles.error}>{messageError}</Text>
        )}

        {/** Picker Selector */}
        <View style={styles.containerPicker}>
          <Text style={styles.label}>Categoría</Text>
          <Picker
            style={styles.picker}
            selectedValue={categoryId}
            onValueChange={value => onChange(value, 'categoryId')}>
            {categories.map(c => (
              <Picker.Item label={c.nombre} value={c._id} key={c._id} />
            ))}
          </Picker>
        </View>

        <Animated.View
          style={{
            ...styles.btns,
            opacity,
            transform: [{translateX: position}],
          }}>
          <Icon
            name="save-outline"
            size={40}
            color={theme.primary.main}
            onPress={saveOrUpdate}
          />
          {_id.length > 0 && (
            <>
              <Icon
                name="camera-outline"
                size={40}
                color={theme.secondary.main}
                onPress={takePhoto}
              />
              <Icon
                name="image-outline"
                size={40}
                color={theme.primary.main}
                onPress={takePhotoFromGallery}
              />
            </>
          )}
        </Animated.View>

        {img.length > 0 && !tempUri && (
          <View style={styles.containerImg}>
            <FadeInImage uri={img} style={styles.img} />
          </View>
        )}

        {tempUri.length > 0 && (
          <View style={styles.containerImg}>
            <FadeInImage uri={tempUri} style={styles.img} />
          </View>
        )}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 10,
    marginHorizontal: 20,
  },

  label: {
    fontSize: 18,
  },
  textInput: {
    borderWidth: 1,
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 20,
    borderColor: 'rgba(0,0,0,0.2)',
    height: 40,
    marginTop: 5,
    color: 'black',
  },
  containerPicker: {
    marginTop: 10,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },

  picker: {
    width: '100%',
  },
  btns: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: 5,
  },
  btn: {
    width: '100%',
  },
  containerImg: {
    shadowColor: '#000',
    shadowOffset: {
      width: 10,
      height: 5,
    },
    shadowOpacity: 0.2,
    shadowRadius: 13.97,
    elevation: 5,
    marginVertical: 40,
  },
  img: {
    borderRadius: 40,
    width: '100%',
    height: 350,
  },
  error: {
    color: 'red',
    fontSize: 15,
  },
});

import React, {
  useContext,
  useLayoutEffect,
  useState,
} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  FlatList,
  RefreshControl,
} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import {StackScreenProps} from '@react-navigation/stack';

import {ProductsStackRootParams} from '../navigator/ProductsNavigator';
import {ProductsContext} from '../context/products/productsContext';
import {theme} from '../theme/appTheme';

interface Props
  extends StackScreenProps<ProductsStackRootParams, 'ProductsScreen'> {}

export const ProductsScreen = ({navigation}: Props) => {
  const {products, loadPorducts} = useContext(ProductsContext);
  const [refreshing, setRefreshing] = useState(false);

  const productItem = ({item}: any) => {
    return (
      <TouchableOpacity
        activeOpacity={0.8}
        style={styles.btn}
        onPress={() =>
          navigation.navigate('ProductScreen', {
            id: item._id,
            name: item.nombre,
          })
        }>
        <Text style={styles.btnName}>{item.nombre}</Text>
      </TouchableOpacity>
    );
  };

  const separator = () => <View style={styles.itemSeparator} />;
  const buttomAddProduct = () => (
    <TouchableOpacity
      activeOpacity={0.8}
      style={{marginRight: 20}}
      onPress={() => navigation.navigate('ProductScreen', {})}>
      <Icon name="add-circle-outline" color={theme.secondary.main} size={40} />
    </TouchableOpacity>
  );

  const loadProductsFromBackend = async () => {
    setRefreshing(true);
    await loadPorducts();
    setRefreshing(false);
  };

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => buttomAddProduct(),
      headerTitleAlign: 'left',
    });
  }, []);

  return (
    <View style={{...styles.container}}>
      <FlatList
        data={products}
        keyExtractor={p => p._id}
        renderItem={productItem}
        showsVerticalScrollIndicator={false}
        ItemSeparatorComponent={separator}
        refreshing={refreshing}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={loadProductsFromBackend}
            title="Cargando.."
            titleColor={theme.primary.main}
            size={50}
            tintColor={theme.primary.main}
            progressViewOffset={50}
            progressBackgroundColor={theme.primary.light}
          />
        }
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 10,
  },
  containerHeader: {
    marginVertical: 10,
  },
  itemSeparator: {
    borderBottomColor: theme.secondary.main,
    borderBottomWidth: 4,
    borderRadius: 20,
    marginVertical: 5,
  },
  btn: {
    marginBottom: 5,
  },

  btnName: {
    color: theme.primary.contrastText,
    fontSize: 20,
    textTransform: 'capitalize',
    opacity: 0.7,
  },
});

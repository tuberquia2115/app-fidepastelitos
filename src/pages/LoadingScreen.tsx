import React from 'react'
import { View, ActivityIndicator } from 'react-native'
import { theme } from '../theme/appTheme';

export const LoadingScreen = () => {
    return (
        <View style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center'
        }}>
            <ActivityIndicator size={50} color={theme.secondary.dark} />
        </View>
    )
}


import React, {useEffect, useState} from 'react';
import {ProductsContext} from './productsContext';
import {Producto} from '../../interfaces/appInterfaces';
import * as serviceProducts from '../../api/services/products';
import {ImagePickerResponse} from 'react-native-image-picker';

interface Props {
  children: JSX.Element | JSX.Element[];
}
export const ProductsProvider = ({children}: Props) => {
  const [products, setProducts] = useState<Producto[]>([]);

  const loadPorducts = async () => {
    try {
      const resp = await serviceProducts.getProducts();
      setProducts([...resp.data.productos]);
    } catch (error) {
      console.error(error);
    }
  };

  const addProduct = async (
    categoryId: string,
    productName: string,
  ): Promise<Producto> => {
    const resp = await serviceProducts.addProduct({
      nombre: productName,
      categoria: categoryId,
    });
    setProducts([...products, resp.data]);

    return resp.data;
  };

  const updateProduct = async (
    categoryId: string,
    productName: string,
    productId: string,
  ) => {
    try {
      const resp = await serviceProducts.updateProduct(productId, {
        nombre: productName,
        categoria: categoryId,
      });

      setProducts(
        products.map(prod => (prod._id === productId ? resp.data : prod)),
      );
    } catch (error) {
      console.error(error);
    }
  };

  const deleteProduct = async (id: string) => {
    try {
      const {
        data: {_id},
      } = await serviceProducts.deleteProduct(id);
      setProducts(products.filter(prod => prod._id !== _id));
    } catch (error) {
      console.error(error);
    }
  };

  const loadProductById = async (id: string): Promise<Producto> => {
    const resp = await serviceProducts.getProductById(id);
    return resp.data;
  };

  const uploadImage = async (data: ImagePickerResponse, id: string) => {
    const assets = data.assets && data.assets.length > 0 ? data.assets[0] : {};
    const fileToUpload = {
      uri: assets.uri,
      type: assets.type,
      name: assets.fileName,
    };

    const formData = new FormData();
    formData.append('archivo', fileToUpload);

    try {
     const resp = await serviceProducts.uploadImage(formData, id);
console.log("respuesra de la subidade la image", resp);
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    loadPorducts();
  }, []);
  return (
    <ProductsContext.Provider
      value={{
        products,
        loadPorducts,
        addProduct,
        updateProduct,
        deleteProduct,
        loadProductById,
        uploadImage,
      }}>
      {children}
    </ProductsContext.Provider>
  );
};

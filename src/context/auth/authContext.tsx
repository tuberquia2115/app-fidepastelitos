import { createContext } from 'react';
import { Usuario, LoginData, RegisterData } from '../../interfaces/appInterfaces';

type AuthContextProps = {
  errorMessage: string;
  token: string | null;
  user: Usuario | null;
  status: 'checking' | 'authenticated' | 'not-authenticated';
  signUp: (register: RegisterData) => void;
  signIn: (loginData: LoginData) => void;
  logOunt: () => void;
  removeError: () => void;
};
export const AuthContext = createContext({} as AuthContextProps);

import React, {useReducer, useEffect} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';

import auth from '../../api/services/auth';
import {AuthContext} from './authContext';
import {authReducer, AuthState} from './authReducer';
import {LoginData, RegisterData} from '../../interfaces/appInterfaces';

interface PropsProvider {
  children: JSX.Element | JSX.Element[];
}

const authInitialState: AuthState = {
  status: 'checking',
  errorMessage: '',
  token: null,
  user: null,
};
export const AuthProvider = ({children}: PropsProvider) => {
  const [state, dispatch] = useReducer(authReducer, authInitialState);

  // iNICIO DE SESSION
  const signIn = async (body: LoginData) => {
    try {
      const {data} = await auth.signIn(body);
      dispatch({type: 'signUp', payload: data});
      await AsyncStorage.setItem('token', data.token);
    } catch (error: any) {
      dispatch({
        type: 'addError',
        payload: error.response.data.msg || 'Información incorrecta',
      });
    }
  };

  //REGISTRAR UN USUARIO
  const signUp = async (obj: RegisterData) => {
    try {
      const {data} = await auth.registerUser(obj);
      dispatch({type: 'signUp', payload: data});
      await AsyncStorage.setItem('token', data.token);
    } catch (error: any) {
      dispatch({
        type: 'addError',
        payload: error.response.data.errors[0].msg || 'Revise la información',
      });
    }
  };

  // CERRAR SESSION
  const logOunt = async () => {
    await AsyncStorage.removeItem('token');
    dispatch({type: 'logout'});
  };

  // REMOVER ERROR
  const removeError = () => dispatch({type: 'removeError'});

  const checkToken = async () => {
    try {
      const token = await AsyncStorage.getItem('token');
      if (!token) return dispatch({type: 'noAuthenticated'});

      const resp = await auth.checkToken();

      // Hay token
      await AsyncStorage.setItem('token', resp.data.token);
      dispatch({type: 'signUp', payload: resp.data});
    } catch (error: any) {
      // no yoken, no autenticado
      if (error.response.status !== 200) {
        return dispatch({type: 'noAuthenticated'});
      }
    }
  };

  useEffect(() => {
    checkToken();
  }, []);

  return (
    <AuthContext.Provider
      value={{
        ...state,
        signUp,
        signIn,
        logOunt,
        removeError,
      }}>
      {children}
    </AuthContext.Provider>
  );
};

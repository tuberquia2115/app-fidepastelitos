export const theme = {
  primary: {
    main: '#F26D91',
    dark: '#bc3b64',
    light: '#E3C7D5',
    contrastText: '#000000',
  },

  secondary: {
    main: '#6451a6',
    dark: '#957ed8',
    light: '#342876',
    contrastText: '#ffffff',
  },
};

import { StyleSheet } from 'react-native';
import { theme } from './appTheme';

const productStyles = StyleSheet.create({
    headerTitleStyle: {
        fontSize: 25,
        color: theme.primary.main,
        textAlign: 'center',
        textTransform: 'capitalize',
        textShadowOffset: {
            height: 0,
            width: 1,
        },
        textShadowColor: theme.primary.main,
        elevation: 1,
    },
});

export default productStyles;

import productsApi from '../../productsApi';
import { ProductsResponse, Producto } from '../../../interfaces/appInterfaces';

const getProducts = async (limite: string = '50') => {
  return await productsApi.get<ProductsResponse>(`/productos?limite=${limite}`);
};

const getProductById = async (id: string) => {
  return await productsApi.get<Producto>(`/productos/${id}`);
};

const addProduct =  async(body: any) => {
 return await productsApi.post<Producto>('/productos', body)
}

const updateProduct = async (productId: string, data: any) => {
return await productsApi.put<Producto>(`/productos/${productId}`, data)
}

const deleteProduct =  async(productId: string) => {
  return await productsApi.delete<Producto>(`/productos/${productId}`);
 }
 

 const uploadImage = async (formData: any, productId: string)  => {
   return await productsApi.put(`/uploads/productos/${productId}`, formData)
 }

export  {
  getProducts,
  getProductById,
  addProduct,
  deleteProduct,
  updateProduct,
  uploadImage
};

import productsApi from '../../productsApi';
import {
  LoginResponse,
  LoginData,
  RegisterData,
} from '../../../interfaces/appInterfaces';

const signIn = async (body: LoginData) => {
  return await productsApi.post<LoginResponse>('/auth/login', body);
};

const checkToken = async () => {
  return await productsApi.get('/auth');
};

const registerUser = async (body: RegisterData) => {
  return await productsApi.post<LoginResponse>('/usuarios', body);
};

export default {
  signIn,
  checkToken,
  registerUser,
};

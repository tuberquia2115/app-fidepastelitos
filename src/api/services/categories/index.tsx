import productsApi from '../../productsApi';
import {CategoriesResponse} from '../../../interfaces/appInterfaces';

const getCategories = async () => {
  
    return await productsApi.get<CategoriesResponse>('/categorias');
  
};

const getCategoryById = async (id: string) => {
  try {
    return await productsApi.get(`/categorias/${id}`);
  } catch (error) {
    console.error(error);
  }
};

export {getCategories, getCategoryById};

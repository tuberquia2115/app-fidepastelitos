import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {ProductsScreen} from '../pages/ProductsScreen';
import {ProductScreen} from '../pages/ProductScreen';
import productStyles from '../theme/productTheme';
import { theme } from '../theme/appTheme';

export type ProductsStackRootParams = {
  ProductsScreen: undefined;
  ProductScreen: {id?: string; name?: string};
};

const Stack = createStackNavigator<ProductsStackRootParams>();

export const ProductsNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: true,
        cardStyle: {
          backgroundColor: 'white',
        },
        headerStyle: {
          elevation: 0,
          shadowColor: 'transparent',
        },
      }}>
      <Stack.Group
        screenOptions={{
          presentation: 'card',
          cardStyle: {
            backgroundColor: 'white',
          },
          headerTitleStyle: productStyles.headerTitleStyle,
          headerTintColor: theme.secondary.dark,
        }}>
        <Stack.Screen
          name="ProductsScreen"
          options={{
            title: 'Productos',
          }}
          component={ProductsScreen}
        />
        <Stack.Screen
          name="ProductScreen"
          options={{
            title: 'Producto',
          }}
          component={ProductScreen}
        />
      </Stack.Group>
    </Stack.Navigator>
  );
};

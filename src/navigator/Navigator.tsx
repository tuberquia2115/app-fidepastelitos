import React, {useEffect, useContext} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import SplashScreen from 'react-native-splash-screen';

import {LoginScreen} from '../pages/LoginScreen';
import {RegisterScreen} from '../pages/RegisterScreen';
import {ProtectedScreen} from '../pages/ProtectedScreen';
import {AuthContext} from '../context/auth/authContext';
import {LoadingScreen} from '../pages/LoadingScreen';
import {ProductsNavigator} from './ProductsNavigator';

const Stack = createStackNavigator();

export const Navigator = () => {
  const {status} = useContext(AuthContext);

  useEffect(() => {
    SplashScreen.hide();
  }, []);

  if (status === 'checking') {
    return <LoadingScreen />;
  }
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        cardStyle: {
          backgroundColor: 'white',
        },
      }}>
      {status !== 'authenticated' ? (
        <>
          <Stack.Screen name="LoginScreen" component={LoginScreen} />
          <Stack.Screen name="RegisterScreen" component={RegisterScreen} />
        </>
      ) : (
        <>
          <Stack.Screen
            name="ProductsNavigator"
            component={ProductsNavigator}
          />
          <Stack.Screen name="ProtectedScreen" component={ProtectedScreen} />
        </>
      )}
    </Stack.Navigator>
  );
};

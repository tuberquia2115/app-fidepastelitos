import { useEffect, useState } from 'react';
import * as serviceCategories from '../api/services/categories';
import { Categoria } from '../interfaces/appInterfaces';

export const useCategories = () => {
    const [hasLoading, setHasLoading] = useState(true);
    const [categories, setCategories] = useState<Categoria[]>([]);

    const getCategories = async () => {
        const { data } = await serviceCategories.getCategories();
        setCategories(data.categorias);
        setHasLoading(true);
    };

    useEffect(() => {
        getCategories();
    }, []);
    return {
        hasLoading,
        categories,
    };
};
